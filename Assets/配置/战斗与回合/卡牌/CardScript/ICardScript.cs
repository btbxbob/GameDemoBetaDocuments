using 模块.战斗与回合.角色与NPC;

public interface ICardScript
{
    /// <summary>
    /// 检查这个卡牌能否使用
    /// </summary>
    /// <returns>能否使用</returns>
    public bool CheckIfCanBeUsed(in CardObject card, in NPCObject target);
    /// <summary>
    /// 实际使用卡牌
    /// </summary>
    /// <returns>返回卡牌使用是否成功</returns>
    public bool Use(ref CardObject card, ref NPCObject target);
}