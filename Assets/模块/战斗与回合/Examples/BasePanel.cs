using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePanel : MonoBehaviour
{
    protected UIManager UImanager;
    
    public UIManager setUiManager
    {
        set
        {
            UImanager = value;

        }
    }
    public virtual void OnInit() { }
    public virtual void OnDestroy() { }
    public virtual void OnExit() { }
    public virtual void OnEnter() { }
    public virtual void OnPause() { }
    public virtual void OnRecovery() { }

}