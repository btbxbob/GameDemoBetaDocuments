using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using 配置.状态配置;

namespace 模块.战斗与回合.角色与NPC
{
    public class Status : ScriptableObject
    {
        [Serializable]
        public class StatusObject
        {
            public StatusConfig Config;
            public int RemainRounds;
        }

        public List<StatusObject> StatusList { get; }

        /// <summary>
        /// 添加状态
        /// </summary>
        /// <param name="obj">状态对象</param>
        public void AddStatus(StatusObject obj)
        {
            StatusList.Add(obj);
        }
    }
}