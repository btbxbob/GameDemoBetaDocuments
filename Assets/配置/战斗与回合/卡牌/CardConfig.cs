using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(menuName = "CardConfig", order = 0)]
public class CardConfig : ScriptableObject
{
    [Searchable]
    public Item[] m_items;

    [System.Serializable]
    public class Item
    {
        [Required] public int ID;
        [Required] public string Name;
        /// <summary>
        /// 由于在Inspector中不方便直接用Interface
        /// 所以用基类包装了一下，实际上实现的是ICardScript里面的方法。
        /// </summary>
        [Required] public CardScript Script;
        [Required] public string Demand;
        [Required] public string Description;
        [Required] public GameObject Effect;
        [Required] public GameObject Icon;
        [Required] public GameObject BgIcon;
        [Required] public string Text;
    }
}