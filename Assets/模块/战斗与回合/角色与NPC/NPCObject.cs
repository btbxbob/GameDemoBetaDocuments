using UnityEngine;

namespace 模块.战斗与回合.角色与NPC
{
    public class NPCObject : MonoBehaviour
    {
        public NPCConfig Config { get; set; }
        public int UID;
        public int HP;
        public string Name;
        public Status Status;
    }
}