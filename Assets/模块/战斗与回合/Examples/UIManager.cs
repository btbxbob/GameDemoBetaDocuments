using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class UIManager: MonoBehaviour
{
    public enum PanelType
    {
        TopPanel,
        HandCards
    }
    /// <summary>
    /// 存储已初始化过的panel
    /// </summary>
    private Dictionary<PanelType, BasePanel> _panelDict = new Dictionary<PanelType, BasePanel>();
    /// <summary>
    /// 存储panel地址
    /// </summary>
    private Dictionary<PanelType, string> _panelPath = new Dictionary<PanelType, string>();
    /// <summary>
    /// 当前显示的uipanel栈
    /// </summary>
    private Stack<BasePanel> _panelStack = new Stack<BasePanel>();
    /// <summary>
    /// 画布坐标
    /// </summary>
    private Transform _canvasTranSform;
    public void Start()
    {
        InitPanelPath();
        _canvasTranSform = GameObject.Find("Canvas").transform;
    }

    public BasePanel PushPanel(PanelType panelType)
    {
        if (_panelDict.TryGetValue(panelType, out BasePanel panel))
        {
            if (_panelStack.Count > 0)
            {
                BasePanel topPanel = _panelStack.Peek();
                topPanel.OnPause();
            }
            _panelStack.Push(panel);
            panel.OnEnter();
            return panel;
        }
        else
        {
            BasePanel panel1 = SpawnPanel(panelType);
            if (_panelStack.Count > 0)
            {
                BasePanel toppanel = _panelStack.Peek();
                toppanel.OnPause();
            }
            _panelStack.Push(panel1);
            panel1?.OnInit();
            return panel1;
        }
    }
    public void PopPanel()
    {
        if (_panelStack.Count == 0) return;
        BasePanel topPanel = _panelStack.Pop();
        topPanel.OnExit();
    }

    public bool checkPanel(PanelType panelType)
    {
        if (_panelDict.TryGetValue(panelType, out BasePanel panel))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private BasePanel SpawnPanel(PanelType panelType)
    {
        if (_panelPath.TryGetValue(panelType, out string path))
        {

            GameObject gameObject = GameObject.Instantiate(AssetDatabase.LoadAssetAtPath<GameObject>(path)) as GameObject;
            gameObject.transform.SetParent(_canvasTranSform, false);
            BasePanel panel1 = gameObject.GetComponent<BasePanel>();
            panel1.setUiManager = this;
            _panelDict.Add(panelType, panel1);

            return panel1;
        }
        else
        {
            Debug.LogError($"{panelType} SpawnPanel Error. Is it in _panelPath?");
            return null;
        }

    }
    private void InitPanelPath()
    {
        // string panelPath1 = "Panel/";
        // string[] path = new string[] { "ShopPanel", "PackagePanel" };
        _panelPath.Add(PanelType.HandCards, "Assets/模块/战斗与回合/Examples/BattleViewTests/panelPrefab.prefab");
        // _panelPath.Add(PanelType.package, panelPath1 + path[1]);
    }


}
