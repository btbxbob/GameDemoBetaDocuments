using UnityEngine;
using 模块.战斗与回合.角色与NPC;

public abstract class CardScript : ScriptableObject, ICardScript
    {
        public abstract bool CheckIfCanBeUsed(in CardObject card, in NPCObject target);
        public abstract bool Use(ref CardObject card, ref NPCObject target);
    }