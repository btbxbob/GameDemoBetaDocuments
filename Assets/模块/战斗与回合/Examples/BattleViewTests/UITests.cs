using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class UITests
{
    [Test]
    public void UIManagerSmokeTest()
    {
        GameObject obj = new GameObject();
        UIManager uim= obj.AddComponent<UIManager>();
        Assert.NotNull(uim);
        GameObject canvasObject = new GameObject("Canvas");
        canvasObject.SetActive(true);
        uim.Start();
        uim.PushPanel(UIManager.PanelType.HandCards);
        uim.PopPanel();
        uim.checkPanel(UIManager.PanelType.HandCards);
    }
}
