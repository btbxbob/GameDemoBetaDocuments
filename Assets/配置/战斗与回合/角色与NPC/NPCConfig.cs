using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "NPCConfig", order = 0)]
public class NPCConfig : ScriptableObject
{
    public enum NPCType
    {
        Player=1,
        Enemy=2,
        Neutral=3
    }

    [System.Serializable]
    public class Item
    {
        [Required]
        public int ID;
        [Required]
        public NPCType Type;
        [Required]
        public string NPCName;
        [Required]
        public NPCScript GetAction;
        [PreviewField]
        public Sprite Icon;
        [Required]
        public int MaxHp;
        [Required]
        public GameObject prefab;
    }
    [Searchable]
    [ListDrawerSettings(NumberOfItemsPerPage =5)]
    public Item[] m_items;
}